str1="""Python is a widely used high-level programming language for
general-purpose programming, created by Guido van Rossum and first released in
1991. An interpreted language, Python has a design philosophy which emphasizes
code readability (notably using whitespace indentation to delimit code blocks rather
than curly braces or keywords), and a syntax which allows programmers to express
concepts in fewer lines of code than possible in languages such as C++ or Java. The
language provides constructs intended to enable writing clear programs on both a
small and large scale .Python features a dynamic type system and automatic memory
management and supports multiple programming paradigms, including object-
oriented, imperative, functional programming, and procedural styles. It has a large and
comprehensive standard library. Python interpreters are available for many operating
systems, allowing Python code to run on a wide variety of systems. CPython, the
reference implementation of Python, is open source software and has a community-
based development model, as do nearly all of its variant implementations. CPython is
managed by the non-profit Python Software Foundation."""

str1 += ' '
w = ''
mydict = {}

for i in range(len(str1)):
    if str1[i].isalpha():
        w += str1[i]
    elif str1[i] == ' ':
        if w == '':
            continue
        
        next_word = ''
        for j in range(i+1, len(str1)):
            if str1[j].isalpha():
                next_word += str1[j]
            elif str1[j] == ' ':
                break
        try:
             mydict[w].append(next_word)
        except KeyError:
            mydict[w] = []
            mydict[w].append(next_word)     
        w = ''

print(mydict)
print('-'*100)
given_word = input('Enter a word -> ')

try:
    value = mydict[given_word]
except KeyError:
    print('No Following Word.')
else:
    small_dict = {}

    for i in value:
        try:
            small_dict[i] += 1 
        except KeyError:
            small_dict[i] = 1

    # print(small_dict)
    
    key = sorted(small_dict,key=lambda v: small_dict[v], reverse=True)[0]
    if key:
        print('Next Predicted Word -> ', key)
    else:
        print('No Following Word.')
