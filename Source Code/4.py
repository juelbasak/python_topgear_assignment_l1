def sorted_func(l):
    return sorted(l, key=lambda x:x[-1])


l1 = [(1, 3), (3, 2), (2, 1)]
l2 = [(1, 7), (1, 3), (3, 4, 5), (2, 2)]

print(sorted_func(l1))
print(sorted_func(l2))