import sys

def steps(start, step, stop):

    # print(f'Source -> {start}, step -> {step}, stopination -> {stop}')

    if abs(start) > (stop):
        return sys.maxsize

    if start == stop:
        return step

    p_axis = steps(start + step + 1, step + 1, stop)


    n_axis = steps(start - step - 1, step + 1, stop)


    return min(p_axis, n_axis)


start = 0
stop = int(input('Enter a Number -> '))
print('Steps Taken -> ', steps(start, 0, stop))
